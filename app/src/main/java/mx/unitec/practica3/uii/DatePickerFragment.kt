package mx.unitec.practica3.uii


import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import java.util.*


class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.DAY_OF_YEAR)
        val month = c.get(Calendar.MONTH)

        return DatePickerDialog(activity, his, year, month, DateFormat.is24HourFormat(activity))
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        Toast.makeText(activity, "${dayOfMonth}:${month}", Toast.LENGTH_LONG).show()
    }


}